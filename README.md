# alphabet-macro

A Rust crate for easy alphabet creation. Documentation is available on [docs.rs](https://docs.rs/alphabet-macro).

## Usage

Add this to your `Cargo.toml`:
```toml
[dependencies]
alphabet-macro = "0.1"
```

## Getting Started

```rs
use alphabet_macro::alphabet;

alphabet!(HEX = "0123456789abcdef");

assert_eq!(HEX.len(), 16);
assert_eq!(HEX[5], '5');
assert_eq!(HEX[10], 'a');
```

## License

This crate is published under the terms of the MIT license. See the LICENSE file for details.